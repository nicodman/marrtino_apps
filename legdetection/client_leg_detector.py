#!/usr/bin/env python

import rospy,time

from sensor_msgs.msg import *
from std_msgs.msg import *
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import PoseWithCovarianceStamped
from geometry_msgs.msg import PoseWithCovariance
from nav_msgs.msg import Odometry

import math

LEG_DETECTOR_TOPIC = '/edge_leg_detector'
BASE_POSE_GROUND_TRUTH_TOPIC = '/base_pose_ground_truth'
PEOPLE_TRESHOLD = 0.9

class LegDetector:

    def __init__(self, leg_detector_topic):
        rospy.Subscriber(leg_detector_topic, PoseArray, self.cb_leg)
        rospy.Subscriber(BASE_POSE_GROUND_TRUTH_TOPIC, Odometry, self.amcl_pose_cb)
        self.marrtino_position_received = False
        rospy.spin()

    def amcl_pose_cb(self, data):
        # Data comes as data.pose.pose.position
        self.marrtino_position = data.pose.pose.position
        self.marrtino_position_received = True


    def cb_leg(self,data):

        if data.poses is None:
            print("NONE")
        else:

            poses = data.poses
            #print(data.poses[0].position)

            if (len(poses) == 1):
                value = -1
            else:
                self.waitForMarrtinoPosition()

                # Compute distances between marrtino and each people.
                distances = list()
                for i in poses:

                    print("marrtino position: %f,%f" %(self.marrtino_position.x,self.marrtino_position.y))
                    print("other position: %f,%f" %(i.position.x,i.position.y))

                    position = math.sqrt(  math.pow(i.position.x,2) + math.pow(i.position.y,2))

                    if position < PEOPLE_TRESHOLD:
                        distances.append(position)

                if (len(distances) > 1):
                    value = 1


                # print(distances)
                # print("\n")

    def waitForMarrtinoPosition(self):
        time.sleep(0.5)
        while not self.marrtino_position_received:
            time.sleep(0.5)

if __name__ == '__main__':
    rospy.init_node('leg_detector_listener', anonymous=True)
    legDetector = LegDetector(LEG_DETECTOR_TOPIC)
