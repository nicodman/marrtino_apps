#!/usr/bin/env python

import sys, time, os
import roslaunch, rospkg


def main():
    # This launches the navigation, detection and tracking nodes
    # rospack = rospkg.RosPack()
    #
    # # rospack.list()
    # print(rospack.get_path('stage_environments'))


    cmd_nav = "roslaunch iri_robot_aspsi tracker_nav_and_detection_for_ASPSI.launch"
    os.system(cmd_nav)
    time.sleep(10)
    #
    # # This launches the accompaniment
    # cmd_accompaniment = "roslaunch iri_robot_aspsi gazebo_ASPSI_OK_ana.launch"
    # os.system(cmd_accompaniment)
    # time.sleep(10)

def send_command(cmd):
    if os.system('tmux -V') == 0:
        sessionname = 'sim'
        r = os.system('tmux select-window -t %s:0' % sessionname)
        if r != 0:
            os.system('tmux -2 new-session -d -s %s' % sessionname)
        r = os.system('tmux select-window -t %s' % name)
        if (r != 0):  # if not existing
            os.system('tmux new-window -n %s' % name)
        os.system('tmux send-keys "%s" C-m' % cmd)
    else:
        os.system('xterm -hold -e "%s" &' % cmd)

if __name__ == '__main__':
    main()