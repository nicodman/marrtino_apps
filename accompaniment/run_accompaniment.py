import sys, time, os

def main():
    # This launches the navigation, detection and tracking nodes
    cmd_nav = "docker exec -it accompaniment bash -ci 'roslaunch iri_robot_aspsi tracker_nav_and_detection_for_ASPSI.launch' "
    os.system(cmd_nav)
    time.sleep(10)

    # This launches the accompaniment
    cmd_nav = "docker exec -it accompaniment bash -ci 'roslaunch iri_robot_aspsi gazebo_ASPSI_OK_ana.launch' "
    os.system(cmd_nav)
    time.sleep(10)


if __name__ == '__main__':
    main()